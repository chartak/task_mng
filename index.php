<?php
// index.php file
include_once("controller/Controller.php");

$controller = new Controller();

//$controller->add();

if(isset($_GET['log']) || isset($_POST['log'])){
    $controller->login();
} elseif(isset($_GET['logout'])) {
    $controller->logout();
} elseif(isset($_POST['insert'])){
    $controller->add();
}elseif(isset($_POST['update'])){
    $controller->update();
} else{
    $controller->invoke();
}