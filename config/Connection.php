<?php

class Connection
{

    private $dbhos='localhost';
    private $dbusername='root';
    private $dbpassword='';
    private $dbname='task_management';
    private $connect;


    public function connect(){
        $this->connect = mysqli_connect($this->dbhos,$this->dbusername,$this->dbpassword,$this->dbname)or die("Errorik".mysqli_error());
        //mysql_select_db($this->dbname , $this->connect)or die("hgfh".mysql_error());

    }

    public function query_db($query){
        mysqli_query($this->connect,"SET CHARSET utf8") or die (mysqli_error());
        $result = mysqli_query($this->connect,$query);
        return $result;
    }

//close db
    function close_db()
    {
        mysql_close($this->connect);
    }

    //last insert id created
    public function id_db()
    {
        return mysqli_insert_id($this->connect);
    }

//		function dbInsertId()
//{
//	return mysql_insert_id();
//}
    //get number affected rows from last query
    function affecte_db()
    {
        $tmp = mysql_affected_rows();
        return($tmp);
    }

    function fetch_assoc_db($result){
        $tmp = mysqli_fetch_assoc($result);
        return $tmp;
    }

    //get number of rows
    function num_rows($q){
        $tmp = mysqli_num_rows($q);
        return $tmp;
    }

    function fetch_row($result) {
        return mysqli_fetch_row($result);
    }
    //get number of fields
    function num_fields($q)
    {
        $tmp = mysql_num_fields($q);
        return($tmp);
    }

    //get mysql info
    function db_info()
    {
        return mysql_info($this->connect);
    }
}