<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" language="javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" class="init">

        $(document).ready(function() {
            $('#example').DataTable({
                //aaSorting: [[0, 'desc']]
                "columnDefs": [
                    { "targets": [2], "orderable": false }
                ],
                "autoWidth": false,
                "lengthChange": false,
                "pageLength": 3,
                searching: false
            });

            $("#myElem").show();
            setTimeout(function() { $("#myElem").hide(); }, 5000);

        } );

    </script>
    <style type="text/css">
        .error-message {
            color:red;
            font-style: italic;
        }
        #myElem {
            color:forestgreen;
            font-size: 20px;
        }
    </style>
</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <h4>Tasks Lists</h4>
            </div>
            <div class="col-md-2">
                <?php if(isset($_SESSION['session_user'])){
                    echo '<a href="index.php?logout=1" class="btn btn-link">'.$_SESSION['session_user'].' Logout</a>';
                  }else{
                    echo '<a href="index.php?log=1" class="btn btn-link">Login admin</a>';
                }?>
            </div>
        </div>
        <form method="post" action="./">
        <div class="row">
            <div class="col-md-12">
                <spam id="myElem"><?php if(isset($task_mess) && $task_mess){ echo 'Task added successfully';}?></spam>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <input type="text" class="form-control" name="username" required  placeholder="user name">
                <spam class="error-message"><?php if(isset($errors['user'])){echo $errors['user'];}?></spam>
            </div>
            <div class="col-md-3">
                <input type="email" class="form-control" name="email" required placeholder="email">
                <spam class="error-message"><?php if(isset($errors['email'])){echo $errors['email'];}?></spam>
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="task_description" required placeholder="task description">
                <spam class="error-message"><?php if(isset($errors['task_desc'])){echo $errors['task_desc'];}?></spam>
            </div>
            <div class="col-md-3">
                <input type="submit" value="insert" name="insert" class="btn btn-primary" id="insertTask">
            </div>
        </div>
        </form>
        <div class="row">
            <div class="col-md-12">
                <table id="example" class="table table-striped table-bordered" style="width:100%">
                    <thead>
                    <tr>
                        <th>User name</th>
                        <th>Email</th>
                        <th>Task description</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    while ($task = mysqli_fetch_row($arrTasks)) {
                        echo '<tr>
                        <td>';
                        if(isset($_SESSION['session_user'])) {
                            echo '<a href="./?task_id=' . $task[0] . '">' . $task[1] . '</a>';
                        } else {
                            echo $task[1];
                        }
                        echo '</td>
                        <td>'.$task[2].'</td>
                        <td>'.$task[3].'</td>
                        <td>'.(($task[5] !=0)? "Fulfillment":"").'<br />'.(($task[5] !=0)? "Edited by admin":"").'</td>
                      </tr>';
                    }
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>
