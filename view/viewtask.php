<html lang="en">
<head>
	<meta charset="UTF-8">
	<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" class="init">

		$(document).ready(function() {

			$("#myElem").show();
			setTimeout(function() { $("#myElem").hide(); }, 5000);

		} );
	</script>
	<style type="text/css">
		.error-message {
			color:red;
			font-style: italic;
		}
		#myElem {
			color:forestgreen;
			font-size: 20px;
		}
	</style>
</head>
<body>
<div class="container">
	<?php if($task){ ?>
		<div class="row">
			<div class="col-md-10">
				<h3>Task details info</h3>
			</div>
			<div class="col-md-2">
				<?php if(isset($_SESSION['session_user'])){
					echo '<a href="./?logout=1" class="btn btn-link">'.$_SESSION['session_user'].' Logout</a>';
				}?>
			</div>
		</div>
<form method="post" action="./">
	<div class="row">
		<div class="col-md-12">
			<spam id="myElem"><?php if(isset($task_mess) && $task_mess){ echo 'Task updated successfully';}?></spam>
		</div>
	</div>
	<div class="row">
		<div class="col-md-3 form-group">User name</div>
		<div class="col-md-3 form-group">Email</div>
		<div class="col-md-3 form-group">Task</div>
		<div class="col-md-3 form-group">Fulfillment</div>
	</div>
	<div class="row">
		<div class="col-md-3 form-group"><input type="hidden" value="<?=$task[0]?>" name="taskId"><?=$task[1]?></div>
		<div class="col-md-3 form-group"><?=$task[2]?></div>
		<div class="col-md-3 form-group">
			<input type="text" class="form-control" name="task_description" value="<?=$task[3]?>">
			<spam class="error-message"><?php if(isset($errors['task_desc'])){echo $errors['task_desc'];}?></spam>
		</div>
		<div class="col-md-3 form-group"><input type="checkbox" value="<?=$task[4]?>" name="completed" <?php if($task[4]==1){ echo 'checked';}?> id="completedTask" /></div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<a href="./" class="btn btn-primary" >Back</a>
			<input type="submit" class="btn btn-primary" name="update" value="Update">
		</div>
	</div>
</form>
	<?php }?>
</div>
</body>
</html>

