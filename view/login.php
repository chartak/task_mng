<?php
?>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript" language="javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script type="text/javascript" class="init">
        $(document).ready(function() {

        } );
    </script>
    <style type="text/css">
        .error-message {
            color:red;
            font-style: italic;
        }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <form method="post" action="../index.php">
                <div class="row">
                    <div class="col-sm-1">Login</div>
                    <div class="col-sm-3"><input class="form-control" type="text" name="username" value="<?=$data['userName']?>"></div>
                    <span class="error-message"><?php if(isset($errors['user'])){echo $errors['user'];}?></span>
                </div>

                <div class="row">
                    <div class="col-sm-1">Password</div>
                    <div class="col-sm-3"><input class="form-control" type="password" name="password"></div>
                    <span class="error-message"><?php if(isset($errors['pass'])){echo $errors['pass'];}?></span>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-1">
                        <input type="submit" value="login" name="log">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



</body>
</html>
