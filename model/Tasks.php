<?php
include_once('./config/Connection.php');

class Tasks {
    private $db_connection;

    public function __construct()
    {
        $this->db_connection = new Connection();
    }

    public function getTaskList()
    {
        $this->db_connection->connect();
        $result = $this->db_connection->query_db("SELECT * FROM tasks");
        return $result;
    }

    public function getTask($id)
    {
        $this->db_connection->connect();
        $result = $this->db_connection->query_db("SELECT * FROM tasks WHERE id=".$id);
        $row = $this->db_connection->fetch_row($result);

        return $row;
    }

    public  function saveTask($arrTask){
        $this->db_connection->connect();

        $query = "insert into tasks (user_name, email, task_description)
                  values('".$arrTask[0]."','".$arrTask[1]."','".$arrTask[2]."')";

        $result = $this->db_connection->query_db($query);

        return $result;
    }

    public  function updateTask($arrTask){

        $this->db_connection->connect();
        $updated_by = ($arrTask[2] == 1)? 1:0;
        $query = "UPDATE tasks SET task_description = '".$arrTask[1]."',
                   status = ".$arrTask[2].", updated_by = ". $updated_by."  WHERE id=".$arrTask[0];

        $result = $this->db_connection->query_db($query);

        return $result;
    }
}
