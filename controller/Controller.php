<?php
include_once("model/Users.php");
include_once("model/Tasks.php");

session_start();
class Controller {
    public $model;
    public $model_user;

    public function __construct()
    {
        $this->model_user = new Users();
        $this->model_tasks = new Tasks();
    }

    public function invoke()
    {
        if (!isset($_GET['task_id']))
        {
            $arrTasks = $this->model_tasks->getTaskList();
            include 'view/tasklist.php';
        }
        else
        {
            if(isset($_SESSION['session_user'])) {
                // show the requested task
                $task = $this->model_tasks->getTask($_GET['task_id']);

                include 'view/viewtask.php';
            } else {
                header('Location: ./');
            }

        }
    }

    public function add()
    {
        $user_name = trim($_POST['username']);
        $email = trim($_POST['email']);
        $task_desc = trim($_POST['task_description']);

        if ($user_name =="") {
            $errors['user'] = 'User name is required';
        } else {

        }

        if($email != ""){
            if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                //InvValid email!
                $errors['email'] = 'Invalid email';
            }
        } else{
            $errors['email'] = 'email is required';
        }

        if($task_desc == ""){
            $errors['task_desc'] = 'Task is required';
        }

        if(empty($errors)) {
            $task[] = $user_name;
            $task[] = $email;
            $task[] = $task_desc;

            $task_mess = $this->model_tasks->saveTask($task);
            $arrTasks = $this->model_tasks->getTaskList();

            include 'view/tasklist.php';
        }else{
            $arrTasks= $this->model_tasks->getTaskList();

            include 'view/tasklist.php';
        }

    }

    public function update()
    {
        if(isset($_SESSION['session_user'])) {
            $task_desc = trim($_POST['task_description']);
            $completed = (isset($_POST['completed']))? 1:0;
            $id = $_POST['taskId'];

            if ($task_desc == "") {
                $errors['task_desc'] = 'Task is required';
            }

            if (empty($errors)) {
                $task[] = $id;
                $task[] = $task_desc;
                $task[] = $completed;

                $task_mess = $this->model_tasks->updateTask($task);
                $task = $this->model_tasks->getTask($id);

                include 'view/viewtask.php';
            } else {
                $task = $this->model_tasks->getTask($id);

                include 'view/viewtask.php';
            }
        } else {
            header('Location: ./');
        }
    }
    public function login()
    {
        $data['userName'] = '';
        if (isset($_POST['username']) && isset($_POST['password']))
        {
            $data['userName'] = trim($_POST['username']);
            $data['password'] = trim($_POST['password']);
            if($data['userName'] ==""){
                $errors['user'] = 'Login is required';
            }
            if($data['userName'] ==""){
                $errors['pass'] = 'Password is required';
            }

            if(empty($errors)){
                $res = $this->model_user->login($data);
                if($res>0){
                    $_SESSION['session_user'] = $data['userName'];
                    $arrTasks = $this->model_tasks->getTaskList();
                    include 'view/tasklist.php';
                } else {
                    $errors['user'] = 'login or password incorrect';
                    include 'view/login.php';
                }
            } else {
                include 'view/login.php';
            }
        }else {
            include 'view/login.php';
        }
    }

    function logout(){
        if(isset($_SESSION['session_user'])){
            unset($_SESSION['session_user']);
        }
        header('Location: ./');
    }
}
